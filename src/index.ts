import express from "express"
import * as controller from "./controller"

const app = express()

app.use(express.json())

app.get('/people', async (req, res) => {
  const { sortBy } = req.query
  res.json(await controller.getPeople(sortBy as string))
})

app.get('/planets', async (req, res) => {
  res.json(await controller.getPlanets())
})

const port = process.env.PORT || 3000

app.listen(port, () =>
  console.log(`🚀 Server ready at: http://localhost:${port}`),
)