const axios = require('axios').default

const SWAPI = "https://swapi.dev/api"

export async function getAll(entity: "people" | "planets"): Promise<any[]> {
  const allEntities: any = []
  let response: any
  let pageParam = "page=1"
  do {
    const start = Date.now()
    response = await axios.get(`${SWAPI}/${entity}/?${pageParam}`)
    console.log(`${pageParam} REQUEST TIME SECONDS: ${(Date.now() - start)/1000}`)
    response.data.results.forEach((entity: any) => {
      allEntities.push(entity)
    })
    const nextPageUrl = response.data?.next || ""
    const pageNumIdx = nextPageUrl.indexOf("?")
    pageParam = nextPageUrl.substring(pageNumIdx + 1)
  } while(pageParam)

  return allEntities
}

export async function getPeopleNamesByUrl(urls: string[]): Promise<any> {
  const requests = urls.map(personUrl => axios.get(personUrl))
  const responses = await Promise.all(requests)
  return responses.map(response => response.data.name)
}
