import * as swapiClient from "./swapi-client"

export async function getPeople(sortBy: string): Promise<any> {
  if (!["name", "mass", "height"].includes(sortBy)) {
    return []
  }
  const start = Date.now()
  const people = await swapiClient.getAll("people")
  console.log("GET ALL PEOPLE REQUEST TIME SECONDS: ", (Date.now() - start)/1000)
  switch(sortBy) {
    case "name" :
      return people.sort((person1, person2) => person1.name < person2.name ? -1 : 1)
    case "height":
    case "mass":
      const peopleWithUnknown = people.filter(person => person[sortBy] === "unknown")
      const sortedPeople = people.filter(person => person[sortBy] !== "unknown")
        .sort((person1, person2) => {
          return parseInt(person1[sortBy].replace(",", "")) - parseInt(person2[sortBy].replace(",", ""))
        })
      return sortedPeople.concat(peopleWithUnknown)
    default:
      return []
  }
}

export async function getPlanets(): Promise<any> {
  const planets = await swapiClient.getAll("planets")
  const planetsWithResidentNames = planets.map(async planet => {
    return {
      ...planet,
      residents: await swapiClient.getPeopleNamesByUrl(planet.residents),
    }
  })
  return Promise.all(planetsWithResidentNames)
}
